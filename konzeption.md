selbstverwaltete, alternative Jugendkultur für alle

![](assets/haus.svg)

# – k o n z e p t i o n –

![](assets/logo.svg) Jugendzentrum Crailsheim e.V.

Erstellt vom Jugendzentrum Crailsheim e.V.<br>
Schönebürgstraße 33<br>
74564 Crailsheim<br>
In Zusammenarbeit mit der Stadtverwaltung Crailsheim<br>
Ressort Soziales und Kultur<br>
Jugendbüro<br>
Maike Engel<br>
07951 9595813

## Einleitung

Die selbstorganisierte Jugendarbeit ist historisch eng mit der Entstehung der klassischen Offenen Kinder- und Jugendarbeit mit hauptamtlichem Personal verknüpft. Ende der 1960er Jahre kam es zur Jugendzentrumsbewegung. Mit Slogans wie ‚Was wir wollen: Freizeit ohne Kontrollen' brachten die Jugendlichen mit ihrer Forderung nach selbstverwalteten Jugendzentren einen neuerlichen Schub für die Offene Kinder- und Jugendarbeit. Besonders auf dem Land, wo das Freizeitangebot am unzureichendsten war, gründeten sich Initiativgruppen. Das Jugendzentrum Crailsheim e.V. wurde 1974 gegründet. Seitdem geht das Juze seinem vorrangigen Zweck, der Jugendarbeit und der Interessensvertretung der Jugendlichen in Crailsheim, nach. Seine Eigenständigkeit hat es sich bis heute bewahrt. Übergeordnet verantwortlich für das Haus ist der Trägerverein Jugendzentrum Crailsheim e.V., ein eingetragener gemeinnütziger Verein entsprechend der gesetzlichen Rahmenbedingungen. Die wechselnde Besetzung des Vereins mit Jugendlichen und jungen Erwachsenen aus Crailsheim kam über die Jahrzehnte in der vielfältigen, unterschiedlichen Gestaltung des Jugendzentrums zum Ausdruck. Verschiedenste Gruppierungen und Jugendliche fanden hier ihr zweites Zuhause, organisierten Arbeitskreise, Kunst, Kultur und Konzerte. Seit seiner Entstehung lebt das Juze von der freiwilligen Gestaltung, Organisation und Mitarbeit aktiver Jugendlicher.

Das Jugendzentrum Crailsheim e.V. ist bereits im Jugendkonzept der Stadt Crailsheim von 1998, damals geschrieben von Erich Beyerbach in Zusammenarbeit mit dem Arbeitskreis Jugendkonzept, als wichtiger Bestandteil der Crailsheimer Jugendarbeit beschrieben.

## Wer wir sind?

### Selbstverwaltung

Konstituierendes Element der Selbstverwaltung ist das ehrenamtliche Engagement junger Menschen. Das Jugendzentrum in Selbstverwaltung ist Anlaufstelle für Jugendliche, die eigenverantwortlich Ideen und Interessen in einem demokratisch selbstverwalteten Rahmen verwirklichen wollen. Ausgangspunkt der Arbeit sind also die jungen Menschen selbst.
„Im Kern entsprechen die von Jugendlichen selbstorganisierten Angebote der Jugendarbeit der reinsten Form der in §11 Abs. 1 SGB VIII genannten Anforderungen der Jugendarbeit. ‚Nicht umsonst gehören Stichworte wie Selbstorganisation. Partizipation und Verantwortungsübernahme unter Gleichaltrigen zu den zentralen Leitbegriffen des Felds.'"[^1] Die Angebote der Jugendarbeit sollen an den Interessen junger Menschen anknüpfen und von ihnen mitbestimmt und mitgestaltet werden, sie zur Selbstbestimmung befähigen und zu gesellschaftlicher Mitverantwortung und zu sozialem Engagement anregen und hinführen. Das Konzept der Selbstverwaltung ermöglicht jungen Menschen einen Erfahrungsraum, indem sie alters- und bedürfnisgerecht aktiv ihr Umfeld mitgestalten dürfen und sollen. Es werden die eigenen Fähigkeiten erprobt, eigene Ideen umgesetzt, Grenzen entdeckt und voneinander gelernt. Bei regelmäßigen Versammlungen wird miteinander diskutiert, Stellung bezogen, Regeln ausgehandelt und sich über gemeinsame Interessen oder Konflikte verständigt. Träger des Jugendzentrums ist der Trägerverein Jugendzentrum Crailsheim e.V. Ehrenamtliche Jugendliche die ‚ihre' Jugendarbeit gänzlich selbst gestalten, agieren ausschließlich ausgehend von ihren Interessen, welche die Grundlage Ihres Engagements bilden. Sie wollen keine direkte Einmischung von Erwachsenen im Sinne der Bevormundung, haben allerdings (zurecht) den Anspruch, dass ein ungestörter Treffpunkt mit Gleichaltrigen im Gemeinwesen einen gleichberechtigten Platz hat und das wichtige Bedürfnis, Ihre Freiräume zu gestalten, respektiert, anerkannt und gefördert wird. In diesem Sinne verstehen die Jugendlichen ‚ihre' Räume durchaus als Dienstleistung für die Gemeinde.

### Team

Selbstverwaltung bedeutet Teamarbeit. Im Jugendzentrum Crailsheim arbeiten Menschen unterschiedlicher Profession miteinander. Es gibt Vereinsmitglieder, Vorstandsmitglieder, Bundesfreiwilligendienstleistende und Honorarkräfte. Die Stadt stellt dem Jugendzentrum ein\*e Mitarbeiter\*in aus dem sozialpädagogischen Bereich zur Unterstützung des Vereins und der pädagogischen Begleitung und Entwicklung der Angebote mit einem Stellenumfang von 100 % zur Verfügung. Es handelt sich dabei um eine komplexe Teamstruktur mit zahlreichen Schnittmengen und Abgrenzungen. Es wird miteinander und selbstständig gearbeitet. Die vielfältige Arbeit ist nicht abhängig von der Vereinsmitgliedschaft. Es braucht die Expertise und Zusammenarbeit der unterschiedlichen Professionen und die zahlreichen Möglichkeiten für interessierte Menschen sich zu engagieren.

#### Der Vorstand

Der Vorstand besteht aus drei Mitgliedern, der\*dem Vorsitzenden, dem\*der Schriftführer\*in und dem\*der Kassierer\*in. Der Vorstand vertritt den Verein gerichtlich und außergerichtlich im Sinne des §26 BGB.

Der Vorstand wird bei der Mitgliedervollversammlung (MVV), die alle eingetragenen Mitglieder umfasst, auf die Dauer eines Jahres gewählt Vorstandsmitglied kann jedes eingetragene Vereinsmitglied werden. Der Vorstand führt die laufenden Geschäfte des Vereins und sorgt für die Umsetzung der Beschlüsse der MVV und unterstützt die Arbeit aller im Haus tätigen im Sinne der in der Vereinssatzung und in der Konzeption festgehaltenen Zielsetzungen. Der\*die Kassierer\*in verwaltet das Vereinsvermögen. Der Verein ist Arbeitgeber. Der Vorstand koordiniert die unterschiedlichen Arbeitsbereiche.

Seine Beschlüsse trifft der Vorstand in Vorstandssitzungen. Aktuell gibt es immer montags 19.30 Uhr die erweiterte Vorstandssitzung, zu der alle Vereinsmitglieder und Interessierte eingeladen sind. Es gibt keinen Zwang zur Mitgliedschaft um teilzunehmen. Der Vorstand fasst die Beschlüsse mit einfacher Mehrheit der erschienenen Mitglieder.

#### Die Ehrenamtler\*innen

Im Schnitt erscheinen 15 bis 20 Ehrenamtler\*innen regelmäßig zur erweiterten Vorstandssitzung. Davon sind einige Menschen Mitglied im Verein, aber nicht alle. Wie oben erwähnt, gibt es keinen Zwang zur Mitgliedschaft, um sich im Jugendzentrum zu engagieren. Die Ehrenamtler\*innen bringen sich mit Ideen und verschiedensten Tätigkeiten ein. Dazu gehören die Instandhaltung des Hauses und der einzelnen Räume, täglich stattfindende Angebote, Konzerte und Veranstaltungen, Ferienangebote und die Landschaftspflege. Je nach zeitlichen Möglichkeiten und persönlicher Motivation bringt sich jede\*r in unterschiedlichem Maße ein. Durch die Vielfalt an Ehrenamtler\*innen, zu denen häufig auch Jahre oder Jahrzehnte später noch vom Verein Kontakt gehalten wird, gibt es einen großen Wissenspool aus dem geschöpft werden kann. Viele Ehrenamtler\*innen haben vertiefte Kenntnisse in verschiedenen Bereichen, die sie dem Hause zur Verfügung stellen. Dazu gehören Veranstaltungstechnik, Layout, Computer, Konzerte, pädagogische Ausbildungen, Umweltschutz, Landschaftspflege, Technik, Holzwerkstatt, Modellbau und künstlerische Tätigkeiten. Erst dadurch ist das vielfältige Angebot im Jugendzentrum überhaupt möglich.

#### Hauptamtliche\*r Mitarbeiter\*in

Im Jugendzentrum ist ein\*eine hauptamtliche Mitarbeiter\*in mit einer Ausbildung aus dem sozialpädagogischen Bereich eingebunden, der\*die fachliche Unterstützung und Beratung leistet. Der\*die hauptamtliche pädagogische Mitarbeiter\*in ist verantwortlich für die Koordinierung der Angebote des Bereichs der Offenen Kinder- und Jugendarbeit. Dazu gehört der Offene Treff mit täglichen Öffnungszeiten, die Gestaltung von Ferienprogrammen, die Begleitung Jugendlicher die Sozialstunden ableisten, Einzelhilfe und verschiedene wechselnde Angebote. Er\*sie garantiert die Kontinuität der Arbeit und ist auch da, wenn die meisten aktiven Jugendlichen arbeiten und lernen müssen.

Zudem leitet er\*sie die Bundesfreiwilligendienstleistende in ihrer Arbeit an. Dazu gehört ein regelmäßiges Treffen, um die zahlreichen Aufgaben zu strukturieren, Einzelgespräche, um Ziele und Projekte zu vereinbaren und zu reflektieren, die pädagogische Anleitung und die Einteilung der Arbeitseinsätze.

Darüber hinaus unterstützt, begleitet und berät er\*sie die Ehrenamtler\*innen, also letztendlich die Selbstverwaltung. In Absprache übernimmt er\*sie Arbeiten, die nur aus ehrenamtlicher Tätigkeit heraus nicht leistbar sind. Dazu gehört beispielsweise die Beschaffung von Geldern über Förderanträge. Die basisdemokratisch geführte erweiterte Vorstandssitzung wird regelmäßig von ihr\*ihm begleitet. Auf diese Weise können Ideen aufgegriffen und Probleme und Konflikte rechtzeitig erkannt und unterstützend begleitet werden. Es findet immer wieder neu ein dialogischer, fachübergreifender Wissenstransfer innerhalb der Offenen Kinder und Jugendarbeit in Theorie und Praxis statt.

#### Honorarkräfte

Eine unterschiedliche Anzahl an Honorarkräften übernehmen einige essentielle Tätigkeiten im Jugendzentrum. Dazu gehören die Begleitung der Angebote für Kinder und Jugendliche in der Werkstatt, die Übernahme kleinerer Reparaturen im Haus, die Landschaftspflege und die Anleitung von Jugendlichen die in der Landschaftspflege Sozialstunden ableisten.
Allein durch eine\*n hauptamtliche\*n Mitarbeiter\*in wären diese Bereiche sonst nicht zu realisieren.

#### Bundesfreiwilligendienst

Im Jugendzentrum gibt es die Möglichkeit einen Bundesfreiwilligendienst zu leisten. Es sind bis zu vier Stellen zu besetzen. Einsatzbereiche sind Natur und Umweltschutz in der Landschaftspflege und das Jugendzentrum ganz allgemein mit Kreativbereich, Werkeln, Veranstaltungen, Offener Treff und die verschiedensten Reparatur- und Renovierungsarbeiten. Die Bundesfreiwilligendienstleistende bringen je nach persönlichen Interessen unterschiedliche Schwerpunkte in die Arbeit im Jugendzentrum ein und sind ein unverzichtbarer Teil des Jugendzentrums. Sie sind über ein Jahr eine wichtige Konstante im Alltag. Sie sind jeden Tag vor Ort und können durch ihr meist jugendliches Alter schnell mit den jugendlichen Besucher\*innen in Kontakt treten und Beziehungen aufbauen.

Gleichzeitig dient der Bundesfreiwilligendienst im Jugendzentrum der persönlichen Entwicklung der jungen Menschen. Sie sind sehr selbstständig in ihrer Arbeit und ihre persönlichen Interessen sind gefragt.

### Lage der Einrichtung

Das Jugendzentrum liegt nahe der Innenstadt Crailsheims am Volksfestplatz. In fünf Minuten ist man in die Innenstadt gelaufen. Der Volksfestplatz dient zugleich als Parkplatz, Veranstaltungsplatz und Treffpunkt für Gruppen älterer Jugendlicher. Zum Volksfest bietet das Jugendzentrum durch seine räumliche Nähe eine ergänzende, alternative Veranstaltung. In Laufnähe befindet sich die Leonhard-Sachs-Schule Gemeinschaftsschule und Grundschule mit derzeit 480 Schülern. Einige Schüler\*innen verbringen die Zeit nach der Nachmittagsschule im Jugendzentrum oder wärmen sich auf während sie auf den Bus warten. In Sichtweite zum Jugendzentrum befindet sich eine Wohngruppe der Lebenswerkstatt. Eine engere Einbindung der Bewohner\*innen im Sinne der Inklusion wird angestrebt. Einzelne Bewohner\*innen sind bereits regelmäßige Besucher\*innen im Jugendzentrum. Das Jugendbüro ist einmal quer über den Volksfestplatz erreichbar und ein wichtiger Kooperationspartner. In enger Absprache ergänzen sich Angebote und Zielgruppen.

Die mannigfaltige Bewohnerstruktur mit einer Vielfalt an kulturellen Hintergründen bedeutet unterschiedliche Klientel für das Jugendzentrum, die von den kostenlosen Freizeitangeboten stark profitieren. Nach 20 Uhr (Schließzeit des Jugendbüros) gibt es in diesem Bereich keinen Treffpunkt für ältere Jugendliche.

### Raumangebot und Ausstattung

Vor der verheerenden Brandstiftung hatte das Jugendzentrum folgende Räumlichkeiten, die zu einer Fortführung der Jugendarbeit in ihrer bisherigen vielfältigen Beschaffenheit unbedingt gebraucht werden:

Herzstück des Jugendzentrums ist der Kneipenraum. Er ist zugleich Ort für Sitzungen, den Offenen Treff, Wohnzimmer, Kneipe mit jugendfreundlichen Preisen, Treffpunkt und Kommunikationszentrum.
Essentiell sind dafür eine Theke, eine Küche, gemütliche Sofas, Stühle und Tische, ein Tischkicker, eine Musikanlage und Regale mit Spielen. Im Jugendzentrum wird nicht nur die Freizeit bestmöglich verbracht, sondern auch dazu beigetragen, dass die Probleme der Jugendlichen mit Eltern, Schule oder Arbeit in Diskussionen bewusst gemacht und vielleicht bewältigt werden können. Dafür ist ein Aufenthaltsraum indem auch lose Gruppen aufeinandertreffen und miteinander in Kontakt kommen können notwendig. Vorhandene Barrieren - z.T. entstanden durch unterschiedliche Erziehung -- können so abgebaut werden.

Ein Konzertraum im Erdgeschoss - mit einer voll funktionsfähigen und einsetzbaren Veranstaltungstechnik ausgestattet - bietet die Möglichkeit regelmäßiger Konzerte, Tanzveranstaltungen und andere Bühnenshows. Die Musik ist in den Kneipenraum über Boxen übertragbar, für diejenigen die bei Veranstaltungen lieber im Kneipenraum sitzen.

Im ersten Stock befinden sich die Werkstatt, ein weiterer Aufenthaltsraum und das Büro des\*der Pädagog\*in und des Vorstands. In der voll ausgestatteten Werkstatt werden Nistkästen und Bienenhotels gebaut, die das Jugendzentrum in Kooperation mit dem Projekt Stadtbienen herstellt und verkauft. Angebote für Kinder und Jugendliche im Bereich Bauen mit Holz sind sehr beliebt und in dieser freien Form in Crailsheim einmalig. Unter fachlicher Anleitung können hier handwerkliche Fähigkeiten erlernt und erweitert werden. Die Werkstatt ist auch ein wichtiger Bestandteil des Ferienangebots. Der große Aufenthaltsraum dient als Rückzugsort und Ruheraum für die Jugendlichen. Es können Hausaufgaben gemacht, Bücher und Zeitungen gelesen werden. Ein Billardtisch ermöglicht den Jugendlichen diesem Hobby außerhalb einer herkömmlichen Kneipe nachzugehen. Aber auch andere Gruppen wie die Lebenswerkstätte nutzen den Raum nach Anmeldung zum Aufenthalt. Durch seine Größe ist der Raum je nach Bedarf zu gestalten. Es ist denkbar, mobile Holztrennwände einzusetzen, um verschiedene Nischen für unterschiedliche Angebote zu schaffen. Für die geschlechtsspezifische Arbeit bietet sich der Raum ebenfalls an. Ein Angebot nur für Jungen oder nur für Mädchen während der allgemeinen Öffnungszeit ist so umsetzbar. Bands brauchen den Raum als Backstage Bereich.

Auf demselben Stockwerk befindet sich das Büro mit dem Arbeitsplatz für den\*die Pädagog\*in und den Vorstand. Hier werden pädagogische Einzelgespräche geführt und bei Bewerbungen unterstützt. Eine schöne Atmosphäre ist wichtig für konstruktive Beratungsgespräche. Neben einem Schreibtisch ist eine Sitzecke mit Tisch und Stühlen notwendig, um sich möglichst auf Augenhöhe begegnen zu können.

Ein schmaler Raum der als Fotolabor genutzt wurde, kann nun Platz für Server und EDV Technik bieten.

Im zweiten Stock ist der hellste Raum des Hauses. Als Café 33 betitelt, bietet der Raum besonders für Kunstschaffende einen Aufenthaltsort.
Durch die zwei Fensterfronten bietet der Raum hervorragende Möglichkeiten für ein Malatelier, aber auch für andere künstlerische Aktivitäten. Verschiedene offene Arbeitsgruppen können hier töpfern, malen, modellieren, batiken, emaillieren u.n.v.m. Es bietet sich an, hier die Mädchen\*arbeit durchzuführen. So kann eine offene Komm-/Gehstruktur umgesetzt werden. Mädchen\* haben hier ihren Schutzraum zu einer bestimmten Zeit in der Woche, können aber nach Bedarf auch in den Kneipenraum runter zum Offenen Treff. Durch eine Theke ist ein eigenes Café Angebot möglich, das eine andere Atmosphäre erzeugt, als im Erdgeschoss.

Ein kleiner Raum im zweiten Stock gegenüber des Kunst-Cafés bietet sich als neues Aufnahmestudio für das Radio Sthörfunk an. Das Radio Sthörfunk, ein nichtkommerzieller Hörfunksender mit Hauptsitz in Schwäbisch Hall, bietet in Kooperation mit dem\*der hauptamtlichen Mitarbeiter\*in Workshops zum Thema Radio an. Redakteure aus Crailsheim nehmen hier ihre Radiosendung auf, die dann im Sthörfunk ausgestrahlt wird. Das Aufnahmestudio wird zugleich als Tonstudio konzipiert, welches von einem Mitarbeiter der Mobilen Jugendarbeit des Jugendbüros betreut werden soll. Hier können Jugendliche aus dem Jugendzentrum und Jugendliche, die noch an keinen Jugendraum angebunden sind, kreativ tätig werden. An den Interessen der Jugendlichen ausgerichtet, gibt es hier die in Crailsheim einmalige Möglichkeit eigene Songs, vor allem aus dem Musikgenre Rap, kostenfrei aufzunehmen.

Der vorhandene Brennofen soll einmal im Monat zugänglich für Hobbytöpfer sein und bietet Kindern und Jugendlichen die seltene, besondere Möglichkeit außerhalb der Schule Keramiken herzustellen und brennen zu lassen.

Der Modellbauraum diente bisher als Mehrzweckraum und wurde für Seminare, Workshops und Kooperationspartner wie Schulen, Vereine, die Volkshochschule, den Jugendgemeinderat und andere zur Verfügung gestellt. Zudem kann er durch seine Größe als Bewegungsraum für Fitnessangebote, Yoga- und Meditationskurse genutzt werden. Es ist angedacht einen Kraftsportraum einzurichten für Jugendliche, für die eine herkömmliche Mitgliedschaft in einem Fitnessstudio zu teuer ist.
Parallel dazu kann hier die geplante Zirkus AG stattfinden. Zirkus ist eine besondere Beschäftigung für Kinder und Jugendliche, da es jedem und jeder die Möglichkeit bietet, teilzuhaben. Ob Jonglage, Teller drehen, Devil Sticks oder Akrobatik, jedes Kind und jede\*r Jugendliche findet etwas, was er\*sie gut kann. Das ist der eigentliche Charme dieses kreativen Themas.

Ein großer Garten gehört ebenfalls zum Jugendzentrum. Kurz vor dem Brand wurde dort begonnen ein Kräuterbeet und ein kleines Gemüsebeet anzupflanzen, als Lern- und Lehrbeet für Kinder. Ein Teil unseres naturpädagogischen Angebots findet dort statt. Es gibt zwei Hochbeete, einen Basketballkorb, mobile Fußballtore können aufgestellt werden und eine Slackline kann gespannt werden. Auch kreative Angebote sollen wieder mit in das Programm aufgenommen werden, wie Graffiti Workshops.

### Finanzierung

Die Stadt stellt dem Jugendzentrum ein Haus zur Verfügung und kommt für die laufenden Kosten und die Instandhaltung auf. Die Jugendarbeit wird mit einem jährlichen Betrag unterstützt und die Lohnkosten für den\*die hauptamtliche Mitarbeiter\*in werden übernommen. Die Jugendarbeit im Jugendzentrum wird größtenteils selbst und über Fördermittel finanziert.
Einnahmen werden durch die Landschaftspflege, Konzerte, Mitgliedsbeiträge und den Verkauf von Nistkästen generiert.

## Arbeitsprinzipien

Damit das Jugendzentrum ein Zentrum der Jugend bleiben kann, müssen die Form und Gestaltung der Inhalte in Selbstbestimmung durch die Jugendlichen selbst geschehen. Folgende Prinzipien müsse dafür gesichert sein:

- Raumplanung und Außengestaltung müssen weitestgehend von den Jugendlichen selbst bestimmt und erarbeitet werden
- Der Verein hat Mitspracherecht bei der Einstellung von hauptamtlichen Mitarbeiter\*innen.
- Das Jugendzentrum muss sich an den Bedürfnissen der Jugendlichen orientieren und von ihnen selbst verwaltet werden.
- Inhalte und Programm werden von den Jugendlichen mitbestimmt.

### Rechtliche Grundlage

Die gesetzliche Grundlage ergibt sich aus dem Kinder- und Jugendhilfegesetz SGB VIII. Die Stadt als öffentlicher Träger der Jugendhilfe ist nach SBG VIII in der Pflicht für ein ausreichendes Angebot der Jugendarbeit zu sorgen. Dem Subsidiaritätsprinzip folgend soll die öffentliche Jugendhilfe von eigenen Maßnahmen absehen, wenn entsprechende Einrichtungen von anerkannten Trägern der freien Jugendhilfe betrieben werden können.

- § 1 SGB VIII: Recht auf Erziehung, Elternverantwortung, Jugendhilfe
- § 2 SGB VIII: Aufgaben der Jugendhilfe
- § 4 SGB VIII: Zusammenarbeit der öffentlichen Jugendhilfe mit der freien Jugendhilfe
- § 11 SGB VIII Jugendarbeit
- § 13 SGB VIII Jugendsozialarbeit
- § 14 SGB VIII Erzieherischer Kinder- und Jugendschutz
- § 81 SGB VIII Zusammenarbeit mit anderen Stellen

Kinder- und Jugendarbeit ist somit Teil der Kinder- und Jugendhilfe.
Zentraler Bestandteil in der Offenen Kinder- und Jugendarbeit ist die Mitbestimmung der Jugendlichen. Die Offene Kinder- und Jugendarbeit hat einen sozialräumlichen Bezug und einen sozialpolitischen, sozialkulturellen und pädagogischen Auftrag. Sie setzt sich dafür ein, dass Kinder und Jugendliche im Gemeinwesen integriert sind.

### Pädagogische Grundhaltung

„Selbstorganisierte Einrichtungen decken den Bedarf von Jugendlichen nach Begegnung, Solidarität, Auseinandersetzung und Erfahrung mit Gleichaltrigen (Peergroups, Cliquen), Gleichgesinnten und Andersdenkenden. Sie bieten Möglichkeiten für selbstbestimmte und konsumunabhängige Freizeit- und Geselligkeitsformen, sowohl im wohnortnahen Lebensumfeld, wie auch im zunehmend wachsenden Begegnungs- und Bewegungsraum."[^2] Das Jugendzentrum stellt für Jugendliche einen Freiraum dar, unabhängig von Schule und Elternhaus, ergänzend für die Sozialisation und Identitätsfindung, Selbsterfahrung und Fremdreflektion, Rollenerfahrung und Rollenaneignung. Freiräume sind unverzichtbar für die Persönlichkeitsentwicklung. Hier finden junge Menschen Raum sich zu erproben und Selbstwirksamkeit zu erfahren. In unserer Gesellschaft mit hoher Komplexität und Wahlfreiheit ist es Voraussetzung, dass junge Menschen handlungsfähig sind. Erwachsene müssen Jugendliche in ihrer Entwicklung begleiten und zum richtigen Zeitpunkt loslassen, so dass sie eigene Erfahrungen sammeln können.

In diesem Sinne bietet das Jugendzentrum Rückzugs- und Freiräume zur Aneignung eigener Kultur, Wertemaßstäbe, Beurteilungskriterien und Erlebnisräume. Als Folge der Selbstorganisation werden die Jugendlichen Teil ihrer Kommune und gestalten diese aktiv mit. Die Konflikte, die zwischen Jugendlichen und Erwachsenen dabei entstehen, sind als etwas Positives und Notwendiges anzuerkennen. Über den Konflikt in Austausch zu treten, ist die Grundlage einer pluralen Gesellschaft.

Der partizipative Raum des Jugendzentrums ist ohne Konsumzwang, frei von Vorurteilen, Diskriminierung, Ausgrenzung und Rassismus, sowie unabhängig von sozialem Statut, kultureller Herkunft, Hautfarbe, Behinderung, Geschlecht und sexueller Orientierung. Solidarisch gegen Menschen- und Fremdenfeindlichkeit. Barrieren sollen abgebaut werden, sowohl im physischen als auch im mentalen Bereich. Wesentliches Prinzip ist die Anerkennung und Wertschätzung von Diversität im Sinne der Inklusion.

Grundlegend orientieret sich das Team des Jugendzentrums an den Prinzipien der Offenen Kinder- und Jugendarbeit:

- Prinzip der Offenheit
- Alle Kinder und Jugendlichen sind willkommen, ohne Voraussetzungen erfüllen zu müssen
- Die Arbeit wird an den Themen und Anliegen der Kinder und Jugendlichen ausgerichtet
- Prinzip der Freiwilligkeit
- Alle Angebote sind freiwillig
- Prinzip der Partizipation
- Kinder und Jugendliche entscheiden über die Angebote und die Gestaltung mit
- Demokratische Lernerfahrung
- Wesentlicher Bestandteil politischer Bildung
- Lebenswelt- und Sozialraumorientierung
- Der umgebende Stadtteil/die Gemeinde wird miteinbezogen
- Lebenserfahrungen, Perspektiven und Deutungen der Kinder und Jugendlichen werden in die Arbeit mit aufgenommen

## Zielgruppenbeschreibung

Da das Jugendzentrum ein sehr breit gefächertes Angebotsspektrum hat, hat es zwei Zielgruppen, die sich nur durch das Alter voneinander abgrenzen. Das sind zum einen Kinder und Jugendliche von 10-16 Jahren sowie Heranwachsende von 16-27 Jahren.

### Kinder und Jugendliche von 10 – 16 Jahren

Diese Zielgruppe kommt vorwiegend aus Crailsheim, einige sind Schüler\*innen der LSS. Es gibt hier massive Unterschiede in den sozialen Lebensbedingungen der Herkunftsfamilie. Ein großer Anteil der Kinder und Jugendlichen hat einen Migrationsanteil und/oder kommt aus sozial benachteiligten Verhältnissen. Viele haben noch nichts gegessen, wenn sie nach der Schule in das Jugendzentrum kommen. Die Lebensverhältnisse einiger Kinder und Jugendlichen aus dem Stadtteil zeichnen sich durch beengten Wohnraum, wenig Spiel- und Lernmöglichkeiten und geringe Förderung durch die Eltern aus. Die Schule wird teilweise als hohe Belastung empfunden. Viele Kinder haben Lernschwierigkeiten. Die mangelnde Konzentrationsfähigkeit macht sich auch nach der Schule in den Angeboten bemerkbar. Die sozialen Kompetenzen sind bei einigen Kindern und Jugendlichen noch entwicklungsbedürftig.

Die Freizeitbeschäftigung dieser Kinder und Jugendlicher ist geprägt durch einen erhöhten Medienkonsum. Diese konsumorientierte Grundhaltung bringen sie auch in das Jugendzentrum mit. Selbst gestalten zu können ist für die meisten eine neue Erfahrung.

### Kinder und Jugendliche von 16 – 27 Jahre

Innerhalb dieser Zielgruppe gibt es die Jugendlichen die sich engagieren und solche, die Besucher\*innen sind.

#### Besucher\*innen ab 16 Jahren

Diese Zielgruppe beinhaltet auch Jugendliche, die aus dem Umkreis von Crailsheim kommen. Dazu gehören Schüler\*innen, Student\*innen, Auszubildende, Berufstätige und Arbeitslose. Da viele bereits ein Auto haben kommen sie oft von weiter her aus den umliegenden Dörfern. Auch hier gibt es Jugendliche aus den unterschiedlichsten sozialen Lebenswelten. Auf der Suche nach einer eigenen Identität sind Erfahrungs- und Experimentierräume wie das Jugendzentrum essentiell.
Häufig empfinden sie Druck durch Schule und Ausbildung und brauchen freie Räume als Ausgleich. Sie leiden oft unter Zukunftsängsten und beruflicher Orientierungslosigkeit. Die Möglichkeit zum Erlernen neuer Verhaltensweisen ist von großer Bedeutung. Das Jugendzentrum bietet einen Ort, an dem die Jugendlichen ihrem Bedürfnis nach kultureller und politischer Teilhabe nachgehen können. Das Gemeinschaftserleben unterstützt die Jugendlichen bei der Bewältigung der in dieser Altersspanne anstehenden Entwicklungsaufgaben.

#### Ehrenamtler\*innen und Vereinsmitglieder ab 16 Jahren

Die Vereinsmitgliedschaft steht jedem Menschen frei. Die Übernahme von Aufgaben und das selbstständige Mitarbeiten beobachten wir eher bei älteren Jugendlichen.

Die sozialen Lebensbedingungen sind ebenso unterschiedlich wie bei den Besucher\*innen. Es gehört eine hohe Bereitschaft zur Übernahme von Verantwortung dazu und die Kapazitäten in seiner Freizeit einer sinnvollen Beschäftigung nachzugehen. Der Übergang von Besucher\*innen zu Ehrenamtler\*innen ist meist fließend. Viele absolvieren einen Bundesfreiwilligendienst und werden danach in ihrer Freizeit im Verein aktiv. Andere waren in ihrer Jugend selbst Besucher\*innen und wollen das nun neuen Generationen von Jugendlichen ermöglichen. Über den Besuch von Konzerten und Veranstaltungen im Jugendzentrum werden ebenfalls neue Mitglieder geworben. Es besteht die Möglichkeit Barschichten zu übernehmen, um so in die Arbeit reinschnuppern zu können. Eine engere Kooperation mit dem Jugendgemeinderat wird angestrebt. Das Jugendforum soll als Bindeglied zwischen Besucher\*innen und Vereinsmitgliedern und Ehrenamtler\*innen dienen.

## Ziele

Das Jugendzentrum ist ein Ort der konsumunabhängigen, aktiven Freizeitgestaltung für Kinder, Jugendliche und junge Erwachsene.
Vorrangiges Ziel der Arbeit des Jugendzentrums sind die Anregung und Befähigung von Jugendlichen weg vom passiven Konsum hin zur aktiven Gestaltung ihrer Freizeit. Dies soll durch die in Kapitel 6 beschriebenen Angebote umgesetzt werden. Dafür braucht es geeignete Räume und Ressourcen, die die meisten Jugendlichen zu Hause nicht zur Verfügung stehen.

Da das Jugendzentrum in Trägerschaft der Vereins Jugendzentrum Crailsheim e.V. ist, sind die übergeordneten Ziele in der Satzung des Vereins festgeschrieben.

Zweck des Vereins ist:

1. Die Trägerschaft, Organisation und Verwaltung des Jugendzentrums.
2. Die Interessen der Jugendlichen der Stadt Crailsheim und Umgebung, in Bezug auf das Jugendzentrum, gegenüber der Öffentlichkeit und gegenüber den Behörden zu vertreten.
3. Die Tätigkeit im Bereich der Jugendpflege und Jugendfürsorge. Gewährleistung einer Jugendarbeit entsprechend den gesetzlichen Vorgaben des Kinder- und Jugendhilfegesetzes.

Es wird davon ausgegangen, dass soziale Probleme, Probleme der Gesellschaft sind und ihre Ursache nicht allein im versagenden Individuum liegen. Um in unserer Gesellschaft bestehen zu können braucht es eine hohe Anpassungsleistung, die viele unserer Besucher\*innen nicht in der Lage sind zu erbringen.

Folgende allgemeine Wirkungsziele leiten sich daraus und aus dem Satzungszweck ab:

Individuelle Ziele:

- Soziales Lernen
- Verhaltensstereotypen sollen aufgebrochen und abgebaut werden, eine Verhaltensvariabilität soll erreicht werden
- Soziale Kompetenzen erwerben und erweitern
- Persönlichkeitsentwicklung durch Empowerment
- Stärkung des Selbstwertgefühls und der Selbstwirksamkeit
- Gruppenerleben: Positive Aspekte von Gruppen sollen gestärkt und gefährdende abgebaut werden
- Fähigkeiten erkennen
- Freizeitangebote für wenig Geld
- Demokratiebildung
- Politisches - Handeln erlernen
- Eigenverantwortliches Handeln und Entscheiden lernen

Institutionelle Ebene:

- Das Jugendzentrum bietet gestaltbare Räume an und fördert so aktive Beteiligung
- Die Interessen der Kinder und Jugendlichen werden in der Gesellschaft vertreten
- Die Angebote werden vom Jugendzentrum an der Lebenswelt, dem sozialraum und dem Wohnort der Kinder und Jugendlichen ausgerichtet

Gesellschaftliche Ebene:

- Demokratiebildung, Auseinandersetzung mit demokratischen Werten und Menschenrechten
- Ermöglicht und fördert aktive Beteiligung an der Gesellschaft
- Soziale Integration
- Verminderung von Ärger und Konflikten mit Jugendlichen im Stadtteil (Müll, Lautstärke)

## Angebote

Um die genannten Ziele zu erreichen, gibt es eine Vielzahl an Freizeitangeboten im Jugendzentrum ohne irgendwelche Kosten für die Kinder und Jugendlichen. Im Folgenden ein Auszug, ohne Anspruch auf Vollständigkeit.

### Offener Betrieb

Im Offenen Betrieb wird Kindern und Jugendlichen zwischen 10 und 16 Jahren bis 18 Uhr die Möglichkeit geboten, sich mit Freunden zu treffen, sich nach der Schule in einem repressionsarmen Raum aufzuhalten, je nach persönlichem Interesse Kicker, Billard oder Gesellschaftsspiele zu spielen oder sich auf unserem Außengelände mit einem Fußball, Badminton oder Basketball sportlich zu betätigen und zu fordern. In der Küche wird gemeinsam gekocht. Die Kinder und Jugendlichen dürfen selbst einkaufen.
Der Nachmittag wird an den Interessen und Bedürfnissen der Kinder und Jugendlichen ausgerichtet oder es gibt ein festes Angebot meistens im Bereich Kochen, Kreativ oder Sport, gegen die Langweile! Ab 16 Jahren geht der Offene Betrieb je nach Wochentag bis spätestens 22 Uhr. Einmal die Woche gibt es von 19-22 Uhr die sogenannte Bufdi-Bar -- ein nichtkommerzielles Kneipenangebot für Jugendliche ab 16, durchgeführt und vorbereitet von den Bundesfreiwilligendienstleistenden.

### Geschlechterbezogene Arbeit

Es wird eine gendersensible Pädagogik gelebt und nicht von einem binären Geschlechtersystem ausgegangen.

#### Mädchen\*arbeit

Im Rahmen der Mädchen\*arbeit gibt es verschiedene Angebote, die sich an den Bedürfnissen der Mädchen\* und jungen Frauen\* ausrichten. Einzelne Mädchen\*aktionstage und einzelne Öffnungstage nur für Mädchen\* sind in Kooperation mit dem Jugendbüro geplant. Die geschlechtsorientierte Arbeit bietet Angebote speziell für Mädchen\*, die Sorgen, Interessen, Fähigkeiten und Wünsche der Mädchen\* werden in den Blick genommen. Die Intention ist es Rollenmuster aufzubrechen und der Auftrag die Mädchen\* und junge Frauen\* zu ermutigen ihren Platz in der Gesellschaft zu finden und zu behaupten. Selbstbewusstsein und Selbstwirksamkeit sollen gestärkt werden, kulturelle Bildung gefördert und Hilfe in der Pubertät und mit der Veränderung und Entwicklung des eigenen Körpers geleistet werden. Mädchen\* tun sich erfahrungsgemäß leichter auch intime Themen wie Sexualität in geschlechtshomogenen Gruppen zu besprechen.

Ehrenamtliche Mitarbeiter\*innen veranstalten jährlich die Veranstaltung Tanzweiber, zu der nur Frauen jeglichen Alters geladen sind. Es ist uns besonders wichtig, die Ideen und Wünsche der Teilnehmerinnen aufzugreifen und nach Möglichkeit umzusetzen.

*\*Wir empfinden als Mädchen, wer sich als Mädchen empfindet. Ganz egal, was andere sagen. Es zählt, was man selbst fühlt. Eigene Identität, eigene Realität.*

#### Jungen\*arbeit

Mit der Holzwerkstatt und den Bewegungsangeboten gibt es viele Bereiche, die erfahrungsgemäß Jungen\* in besonderem Maße ansprechen. Im Offenen Betrieb gibt es durch die Vielzahl an Räumen gute Möglichkeiten Jungen\* gesondert anzusprechen und ihnen Rückzugsräume zu ermöglichen. Fragen und Erfahrungen zu dem Rollenverständnis von Jungen\* und Männern\* in unserer Gesellschaft, zu Körper und Gesundheit und zu Sexualität lassen sich in geschlechtshomogenen Gruppen häufig einfacher bearbeiten.

*\*Wir empfinden als Jungen, wer sich als Jungen empfindet. Ganz egal, was andere sagen. Es zählt, was man selbst fühlst. Eigene Identität, eigene Realität.*

### Kunst-Café

Im Kunstcafé können Besucher\*innen jederzeit während des Offenen Betriebs malen, basteln, Kunst entdecken. Jede\*r kann etwas! Zudem gibt es eine Kreativ-AG einmal die Woche. Die über Jahrzehnte gewachsene Ausstattung macht auch seltene Hobbies im kreativen Bereich möglich, wie beispielsweise töpfern. Einmal im Monat soll der Brennofen für Hobbytöpfer aus und um Crailsheim angeheizt werden, da die meisten keinen direkten Zugang zu einem haben. Sofas zum gemeinsamen verweilen und plaudern runden die Atmosphäre gemütlich ab. Kreative und künstlerisch begabte Jugendliche gehören schon immer zum Besucherkreis des Jugendzentrums. Dies spiegelt sich auch in der Gestaltung des Jugendzentrums wieder. Mit seinen zwei Fensterfronten bietet das Kunstcafé hervorragende Möglichkeiten für ein Malatelier. Durch die Ausstattung mit vielen Tischen sind auch weitere Angebote im kreativen Bereich wie Nähen oder Basteleien aller Art möglich.

### Holzwerkstatt

Die Holzwerkstatt wird von Honorarkräften geführt. Sowohl im Alltag des Jugendzentrums als auch bei Ferienprogrammen gibt es hier vielfältige Angebote zum Werkeln mit Holz oder Modellbau. Es wurden schon Schlaginstrumente gebaut, ein Piratenschiff und zahlreiche Bienenhotels.
An einem Tag in der Woche können straffällig gewordene Jugendliche mitarbeiten. Kleinere Reparaturen am Haus können ebenfalls eigenständig von den Mitarbeiter\*innen des Jugendzentrums übernommen werden. Falls Jugendliche mutwillig oder ausversehen Teile der Einrichtung kaputtmachen, kann direkt Wiedergutmachung geleistet werden, indem sie den entsprechenden Gegenstand selbst reparieren müssen. Das alles ist durch die sehr gute Ausstattung der Holzwerkstatt mit kleinen und großen Geräten möglich. Die Holzwerkstatt wird regelmäßig von anderen Einrichtungen angefragt, um dort mit Kindern und Jugendlichen werkeln zu können, was wir sehr begrüßen. Auch mit Schulen gibt es Kooperationen im Nachmittagsangebot.

### Medienangebot

Den Kindern und Jugendlichen stehen vier Laptops zur Verfügung, die sie nach Bedarf nutzen dürfen. Hier können sie unter Anleitung eines\*einer pädagogischen Mitarbeiter\*in den Umgang mit einem Computer und dem Internet erlernen.

Im Konzertraum besteht die Möglichkeit einer Einführung in die Veranstaltungstechnik. Es ist möglich einen DJ-Pass zu machen, dann können die Kinder und Jugendlichen selbst Musik auflegen.

Der Verein Freies Radio Sthörfunk e.V. soll wieder mit einem Radiostudio in das Jugendzentrum einziehen. Dort finden Workshops statt und Kinder und Jugendliche haben die Möglichkeit eine eigene Radiosendung oder einen Podcast aufzunehmen. Zudem wird das Aufnahmestudio zu einem Tonstudio erweitert, in dem die Jugendlichen ihre (Rap-)Songs aufnehmen können.

Ein Schneidplotter ist sehr beliebt und bietet die besondere Möglichkeit selbst T-shirts, Kissen, Taschen und vieles mehr mit eigenen Motiven zu bedrucken. Das wird sowohl bei Ferienprogrammen als auch für einzelne Angebote und Workshops eingesetzt. Es können besonders Interessierten werden grundlegende Fachkenntnisse in einem Bildbearbeitungsprogramm vermittelt werden.

### Naturpädagogisches Angebot

Das Jugendzentrum legt einen großen Schwerpunkt auf den Bereich Umweltschutz. Sowohl im eigenen tätig werden, als auch in der Vermittlung. Unsere Ziele beziehen sich auf die Kompetenzen der Kinder und Jugendlichen im Bereich der Naturwahrnehmung, als auch die Vermittlung handwerklicher Tätigkeiten. Durch die Umwelttage, die jährlich unter einem anderen Motto stattfinden, sollen Kinder und Jugendliche an die Natur herangeführt werden, frei nach dem Motto ‚Nur was man kennt das schützt man'. Jeden Monat gibt es mindestens ein Angebot zum Thema Natur und Umwelt. Das sind Angebote wie Tierbeobachtung, Spuren suchen, Vogelstimmen erkennen, Insekten erforschen, aber auch kleine Workshops wie Nistkästenbau, Vogelfutter selber machen oder Apfelsaft selbst pressen.

### Ferien

Das Jugendzentrum beteiligt sich aktiv an und ist Kooperationspartner bei verschiedenen Ferienangeboten, die das Jugendbüro ausrichtet. Dazu gehören unter anderem die Kinder- und Jugendkulturwoche, die Regenbogentage und der Talentcampus.

In den Sommerferien findet die Kunstwoche statt. Ein buntes, ganztägiges Kreativangebot für Kinder und Jugendliche, die eine Woche lang Kunst und Kultur aus verschiedenen Perspektiven kennenlernen und selbst tätig werden können.

### Projekt mit straffälligen Jugendlichen

Das Jugendzentrum arbeitet seit Jahren eng mit der Jugendgerichtshilfe zusammen. Unserer Erfahrung nach gibt es einen kontinuierlichen Bedarf an Plätzen für straffällige Jugendliche, die Stunden in einer gemeinnützigen Einrichtung leisten müssen. Im Jugendzentrum werden sie dabei nicht nur in verschiedenen Bereichen eingesetzt, sondern auch pädagogisch begleitet. In Einzelgesprächen wird eine Reflexion der Straftat und eine damit verbundene Einnahme der Opferperspektive angestrebt. Der\*die Jugendliche kann sowohl Flexibilität durch neue Anforderungen, als auch Zuverlässigkeit lernen. Durch die Kontakte in der offenen Jugendarbeit und der mobilen Jugendarbeit gibt es immer wieder Einzelfälle an Jugendlichen, die eine kurzfristige, niederschwellige Beschäftigung suchen, um eine finanzielle Notlage zu mindern.

Über Jahre hinweg hat das Jugendzentrum Pflegeverträge mit dem Landschaftserhaltungsverband und führt auch für die Stadt Crailsheim, sehr spezielle, kleinräumige und personal- intensive Pflegemaßnahmen durch. Dies geschieht und geschah auch ganz aktuell auf neun verschiedenen Flächen auf Crailsheimer Gemarkung. Die Landschaftspflege ist in enger Verknüpfung mit unserem naturpädagogischen Angebot der oben genannten Umwelttage zu betrachten. Das Jugendzentrum verrichtet mit den „Mobilen Hilfen" verschiedene Arbeiten in Crailsheim. Zu nennen sind hier Auf- und Abbauarbeiten von Ferienprogrammen wie der Stadtranderholung, der Kunstwoche und der Regenbogentage. Zudem werden Transporte und Fahrdienste mit unserem Sprinter für Einrichtungen der Kinder- und Jugendarbeit ausgeführt. Diese ‚Mobilen Hilfen' sind je nach Bedarf und personeller Besetzung im Jugendzentrum erweiterbar. Ein schöner Beitrag ist seit Jahren der Bau von Nisthilfen für Insekten, meist als Bienenhotels bezeichnet, im Rahmen des Projekts Stadtbiene, an dem sich das Jugendzentrum gerne beteiligt. Zur Stadtführung der Laga-Kommission hat sich das Jugendzentrum mit einem Wildbienenhoraff in der Jagstaue beteiligt. Die straffällig gewordenen Jugendlichen erleben hier eine sinnvolle Tätigkeit, eine Integration in die Gemeinschaft und sie erlernen handwerkliche Fähigkeiten.

### Lebenspraktische Hilfen

Jede\*r Besucher\*in bekommt bei Bedarf Unterstützung bei Hausaufgaben, Lernen oder Bewerbung schreiben. Einmal die Woche gibt es eine feste Lerngruppe mit den Bundesfreiwilligendienstleistenden. Aber auch bei Konflikten, Probleme mit den Eltern oder in der Schule finden die Kinder und Jugendlichen im Jugendzentrum Unterstützung. Die alltägliche Arbeit mit den Jugendlichen ist geprägt durch die Vermittlung von Normen und Werten und die Begleitung der Entwicklung zu mündigen Bürger\*innen.

### Einzelfallhilfe

Ein Kind oder Jugendliche\*r kann auch im Mittelpunkt der sozialpädagogischen Intervention stehen. Im Jugendzentrum wird Einzelfallhilfe in vielfältiger Form betrieben. Dies kann das Schreiben einer Bewerbung, aber auch das Initiieren von Hilfeprozessen in Verknüpfung mit anderen Hilfesystemen sein. Hierbei ist die Beziehungsarbeit die Grundlage der Arbeit.

### Selbstverwaltung

Sämtliche genannten Angebote sind durch die Zusammenarbeit aller genannter Beteiligter erst möglich. In den erweiterten Vorstandssitzungen werden Angebote und das Ferienprogramm besprochen.
Der\*die hauptamtliche Mitarbeiter\*in ist für die pädagogische Leitung zuständig. Die vielfältigen Inhalte kommen zu einem großen Teil von allen Beteiligten. Das hohe Engagement der einzelnen Ehrenamtler\*innen und der Honorarkräfte, entsteht neben der persönlichen Motivation durch das Gefühl der Selbstwirksamkeit. Wer im Jugendzentrum mitarbeitet, hat das Gefühl einen Beitrag leisten zu können zur Gesellschaft und gleichzeitig eigene Ideen umsetzen und verwirklichen zu können. Die bemerkenswerten Gestaltungsmöglichkeiten führen zu einer hohen Identifikation mit dem Gebäude, was Jugendliche langfristig an das Jugendzentrum bindet und einen pfleglichen Umgang damit fördert. Die Selbstverwaltung bietet besondere Entwicklungsmöglichkeiten, besonders im Hinblick auf demokratische Bildung. Demokratische Prozesse können hier geübt und gelernt werden. Jugendliche lernen eigene Interessen zu erkennen, auszuformulieren und vorzubringen, eigene Regeln in ihrer Notwendigkeit zu erkennen, auszuhandeln und verbindlich aufzustellen, Kritikfähigkeit, Andersdenkende zu respektieren, soziale Verantwortung zu übernehmen und dass ihre individuellen Interessen gehört werden und Beachtung finden können.

#### Jugendforum

In einer wöchentlich stattfindenden Versammlung aller interessierten Besucher\*innen können Kinder und Jugendliche teilnehmen, die noch zu jung für die Mitwirkung im Verein sind oder an einer Mitwirkung mit geringerer Verbindlichkeit interessiert sind. Das Jugendforum soll als Übergang vom konsumierenden Besucher zum aktiven Mitwirkenden dienen.
Hier haben die Kinder und Jugendlichen die Möglichkeit zu einem Ideenaustausch, der Absprache von Aufgaben und zur Klärung möglicher Konflikte und Probleme. Jugendliche können sich auch hier niedrigschwellig mit ihren Bedürfnissen und Ideen zur Freizeitgestaltung einbringen. Tagesordnungspunkte werden zu Beginn der jeweiligen Sitzung gesammelt und jede\*r Jugendliche hat die Möglichkeit, eigene Anliegen einzubringen. So können die Besucher\*innen auf die Angebotswahl und auf manche Regeln Einfluss nehmen. Es kann ein\*e Sprecher\*in gewählt werden, der\*die die Themen in die Vorstandssitzung einbringt. Das Jugendforum ist öffentlich, jede\*r Jugendliche, unabhängig davon ob er\*sie sich im Haus engagiert oder nicht, kann sich einbringen und mitdiskutieren oder sich informieren. Begleitet und unterstützt wird das Jugendforum durch eine\*n Sozialarbeiter\*in und die Bundesfreiwilligendienstleistende\*innen.

#### Jugendkulturarbeit

Die Konzerte und Veranstaltungen sind ein wichtiger Beitrag des Jugendzentrums zum kulturellen Leben in Crailsheim. Regelmäßig werden Bands aus ganz Deutschland und Europa eingeladen, hier im Jugendzentrum zu spielen. So werden Nachwuchsbands gefördert und auf die Musikszene der Jugendlichen eingegangen. Wer eine bestimmte Musikrichtung hört, die in einer Kleinstadt kaum vertreten ist, kann selbst die Lieblingsband herholen. Das Jugendzentrum stellt die Räumlichkeiten und das Equipment.
Die Vorbereitung und Durchführung der Konzerte oder Vorträge übernehmen die Jugendlichen selbst. Dadurch können sie Fähigkeiten erlernen wie Zeitmanagement, Organisationsfähigkeit, Verantwortungsübernahme und Zuverlässigkeit. Auch zum Volksfest leistet das Jugendzentrum mit seiner Veranstaltung einen wichtigen Beitrag.

#### Wiederkehrende Projekte

Um Spenden zu sammeln und einen weiteren Beitrag zu Nachhaltigkeit und Umwelt zu leisten plant das Jugendzentrum viermal jährlich eine Kleidertauschparty, bei der gespendete Kleidung gegen Spende erhältlich ist und es ein geselliges Beisammensein mit Punsch, Kuchen und Essen geben soll.

Ein Repair-Café soll regelmäßig umgesetzt werden. Menschen aus Crailsheim und Umgebung können Haushaltsgeräte, Fahrräder oder Kleidung mitbringen, die gemeinsam repariert wird. Durch die handwerklich begabten Vereinsmitglieder und Honorarkräfte ist das möglich.

In einigen größeren Städten gibt es eine sogenannte Küfa (Küche für alle). Die Idee dahinter, ist das gemeinsame Zubereiten und Kochen von Gerichten, die für wenig Geld ausgegeben werden können. Die Küfa bietet gleichzeitig die Erfahrung von Gemeinschaft, denn gemeinsames Kochen verbindet, ein Lernfeld für Jugendliche im Bereich der Gesundheit und Alltagskompetenzen, eine weitere kleine Einnahmequelle für das Jugendzentrum und die Möglichkeit der kulturellen Integration. Menschen mit Migrationshintergrund können Gerichte aus ihren Herkunftsländern zeigen und so Barrieren zwischen den Kulturen abbauen.

## Institutionsbezogene Arbeitsschwerpunkte

### Öffentlichkeitsarbeit

Um eine höchstmögliche Transparenz zu erreichen und die Jugendlichen in ihrer Lebenswelt abzuholen, legt das Jugendzentrum großen Wert auf seine Öffentlichkeitsarbeit. Über die social Media Kanäle wird zu Aktionen aufgerufen und darüber berichtet. Eine Homepage bündelt alle Informationen zu Öffnungszeiten, Team, Verein und Angeboten. Das Programm wird zusätzlich mit Flyern beworben.

### Vernetzung und Kooperationen

Primärer Kooperationspartner des Jugendzentrums ist das Jugendbüro.
Durch die sich überschneidenden Angebote und Zielgruppen ist es in beidseitigem Interesse eng zusammenzuarbeiten. Die Kooperation reicht von der gemeinsamen Arbeit und Planung der pädagogischen Angebote im Jugendzentrum bis zur Entwicklung gemeinsamer Projekte.

Es gibt Kooperationen mit Schulen im Nachmittagsprogramm, die Lebenswerktstätte nutzt die Räumlichkeiten des Jugendzentrums zum Vesper und es gibt häufig - in Ermangelung anderer Anlaufstellen - Anfragen unterschiedlicher Einrichtungen die Holzwerkstatt für Angebote mit Kindern und Jugendlichen zu nutzen. Zudem kooperiert das Jugendzentrum mit der Gruppe Fairteiler, die Lebensmittel retten.

Vernetzungen gibt es mit der Jugendsuchtberatung, der Jugendgerichtshilfe, der Volkshochschule und anderen Jugendhilfeeinrichtungen.

Das Jugendzentrum versteht sich als Jugend- und Kulturzentrum, das seinen Teil zu einem regionalen Wandel beiträgt. Es ist ein offenes Haus, alle die sich einbringen wollen und mit der Satzung des Vereins identifizieren können, sind willkommen. Es bietet Raum für verschiedene Organisationen, Gruppen, Zusammenschlüsse und Vereine. Menschen treten in diesem Haus in Verbindung, die sich sonst in ihrem Alltag nicht begegnet wären und tragen so gemeinsam zu mehr ökologischen und sozialen Strukturen bei.

## Quellenangaben

KVJS Berichterstattung: Kinder- und Jugendsozialarbeit auf kommunaler Ebene in Baden-Württemberg/Berichterstattung 2019.

Bundesministerium für Familie, Senioren, Frauen und Jugend 16. Kinder- und Jugendbericht: Förderung demokratischer Bildung im Kinder- und Jugendalter.

Rahmenbedingungen und Strukturqualität/Offene Kinder- und Jugendarbeit in Baden-Württemberg: Arbeitsgemeinschaft Jugendfreizeitstätten Baden-Württemberg e.V., Stuttgart 2020.

[^1]: Jahresbericht KVJS

[^2]: KVJS Berichterstattung: Kinder- und Jugendsozialarbeit auf kommunaler Ebene in Baden-Württemberg/Berichterstattung 2019, S. 90.
