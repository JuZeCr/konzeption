# Konzeption

[Dies](konzeption.md) ist die Quelldatei, die zu unserer aktualisierten Konzeption geführt hat.
Das ursprüngliche Konzept war bereits über 40 Jahre alt und wurde nun durch [dieses Dokument](konzeption.pdf) ersetzt.

Um von anderen Einrichtungen (vor allem Jugendhäusern) beim Schreiben einer eigenen Konzeption genutzt werden zu können, stellen wir unsere Dokumente unter einer offenen Lizenz zur Verfügung. Lediglich die Grafiken sind hiervon ausgenommen.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz</a>.

Um von der Markdown-Quelldatei zum letztendlichen PDF zu kommen, wird zunächst nach ODT gewandelt:

    pandoc -s konzeption.md --output konzeption.odt

Die ODT-Datei kann anschließend mittels LibreOffice formatiert werden.
Anschließend wird das letztendliche PDF generiert (ein Export als PDF direkt aus LibreOffice heraus ist auch möglich):

    libreoffice --headless --convert-to pdf konzeption.odt

Bitte beachten, dass keine _inhaltlichen_ Änderungen an der ODT-Datei vorgenommen werden, da diese sonst verloren wären.
